import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-my-burger-vlada.firebaseio.com/'
});
export default instance;
